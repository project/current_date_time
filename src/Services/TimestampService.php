<?php

namespace Drupal\current_date_time\Services;

use Drupal\Core\Datetime\DateFormatterInterface;

class TimestampService
{

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
    protected $dateFormatter;

  /**
   * Constructs a new TimestampService object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
    public function __construct(DateFormatterInterface $date_formatter)
    {
        $this->dateFormatter = $date_formatter;
    }

  /**
   * Get formatted timestamp.
   *
   * @param int $timestamp
   *   The timestamp to format.
   * @param string $format
   *   The desired date format.
   * @param string $timezone
   *   The timezone identifier.
   *
   * @return string
   *   The formatted timestamp.
   */
    public function getFormattedTimestamp($timestamp, $format, $timezone)
    {
        return $this->dateFormatter->format($timestamp, 'custom', $format, $timezone);
    }
}
