#  Current date and time

This module is developed for showing the current date and time according to different timezone.
It is a simple custom block showing the current date and time as selected timezone.
Users can easily configure and customize the displayed time format.

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module.

## Configuration

The module has no menu or modifiable settings. There is no configuration.

## Maintainers

