<?php

namespace Drupal\current_date_time\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\current_date_time\Services\TimestampService;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a custom date and time block.
 *
 * @Block(
 *   id = "zone_block",
 *   admin_label = @Translation("Zone Block"),
 *   category = @Translation("Custom")
 * )
 */
class ZoneBlock extends BlockBase implements ContainerFactoryPluginInterface
{

  /**
   * The timestamp service.
   *
   * @var \Drupal\current_date_time\Services\TimestampService
   */
    protected $timestampService;



  /**
   * Constructs a new TimeBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\current_date_time\Services\TimestampService $timestamp_service
   *   The timestamp service.
   */
  
    public function __construct(array $configuration, $plugin_id, $plugin_definition, TimestampService $timestamp_service)
    {
        parent ::__construct($configuration, $plugin_id, $plugin_definition);
        $this->timestampService = $timestamp_service;
    }

  /**
   * {@inheritDoc}
   */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get("current_date_time.timestamp_service"),
        );
    }

  /**
   * {@inheritdoc}
   */
    public function build()
    {
        $config = $this->getConfiguration();
        $selectedCountry = $config['country'] ?? 'United States';
        $date_time_format = $config['date_time_format'];
        $current_timestamp = REQUEST_TIME;
        $timezone = $config['country'];

      // Use the custom timestamp service.
        $formatted_timestamp = $this->timestampService->getFormattedTimestamp($current_timestamp, $date_time_format, $timezone);
        $build = [
        '#markup' => '<div id="custom-time-block">' . t('Date and Time: @datetime<br>Country: @country', [
        '@datetime' => $formatted_timestamp,
        '@country' => $selectedCountry,
        ]) . '</div>',
        '#cache' => [
        'context' => [],
        'tags' => [],
        ],
        ];
        return $build;
    }

  /**
   * {@inheritdoc}
   */
    public function blockForm($form, FormStateInterface $form_state)
    {
        $form = parent::blockForm($form, $form_state);
        $config = $this->getConfiguration();
        $selectedCountry = $config['country'] ?? 'United States';
        $form['date_time_format'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Date And Time Format'),
        '#default_value' => $config['date_time_format'] ?? 'd/m/Y [g:i:s A]',
        ];
        $form['country'] = [
        '#type' => 'select',
        '#title' => $this->t('Country'),
        '#options' => system_time_zones(),
        '#default_value' => $selectedCountry,
        ];
        return $form;
    }

  /**
   * {@inheritdoc}
   */
    public function blockSubmit($form, FormStateInterface $form_state)
    {
        $this->setConfigurationValue('date_time_format', $form_state->getValue('date_time_format'));
        $this->setConfigurationValue('country', $form_state->getValue('country'));
    }

    public function getCacheMaxAge()
    {
        return 10;
    }
}